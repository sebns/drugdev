#!/usr/bin/env python3
"""
Entry point for celery workers.
"""
from app import create_app
from app.celery_creator import init_celery_app, celery_app


app = create_app()
init_celery_app(celery_app, app)
