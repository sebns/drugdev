#!/usr/bin/env python3
"""
Entry point for Flask app
"""
from app import create_app

app = create_app()

if __name__ == "__main__":
    import os
    app.run(host='0.0.0.0', port=os.environ['FLASK_PORT'])
