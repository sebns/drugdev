"""Base testing module that other tests extend from."""
from flask_testing import TestCase

from app import db, create_app as flask_create_app


class BaseTestCase(TestCase):
    """A base test case."""
    URL_BASE = 'http://0.0.0.0'

    def create_app(self):
        """Create app for tests."""
        return flask_create_app()

    def setUp(self):
        """Setup tests."""
        db.create_all()

    def tearDown(self):
        """Tear down tests."""
        db.session.remove()
        db.drop_all()
