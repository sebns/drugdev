"""Unit tests for user module."""
from flask import url_for, Response
import logging
from urllib.parse import quote

from .base import BaseTestCase

logger = logging.getLogger(__name__)


class EndpointsTestCase(BaseTestCase):
    """Test REST API endpoints related to users."""

    def test_api_doc(self) -> None:
        endpoint = url_for('api.doc')
        logger.info(f'Testing endpoint: {endpoint}')
        response = self.client.get(self.URL_BASE + endpoint)
        self.assert200(response)

    def test_create_user(self) -> None:
        """Test creating a user."""
        response = self._create_user('user1')
        self.assert200(response)

    def test_create_same_user_twice(self) -> None:
        """Create user with already existing username."""
        self._create_user('user1')
        response = self._create_user('user1')
        self.assertStatus(
            response, 409, "Creating user with existing username should fail")

    def test_get_user_info(self) -> None:
        """Test getting user info"""
        self._create_user('user1')
        response = self._get_user_info('user1')
        self.assert200(response)

    def test_get_unknown_user_info(self) -> None:
        """Test getting user info of unknown user"""
        self._create_user('user1')
        response = self._get_user_info('user2')
        self.assert404(response, "Getting info of unknown user should fail")

    def _create_user(self, username: str) -> Response:
        """Create user via REST API."""
        params = [
            'first_name=joe',
            'last_name=doe',
            'emails=aaa@gmail.com,bbb@gmail.com',
        ]
        query = '&'.join([quote(p) for p in params])
        endpoint = f"/api/v1.0/users/{username}?{query}"
        logger.debug(f'Testing endpoint: POST {endpoint}')
        return self.client.post(self.URL_BASE + endpoint)

    def _get_user_info(self, username: str) -> Response:
        """Get user info via REST API."""
        endpoint = f"/api/v1.0/users/{username}"
        logger.debug(f'Testing endpoint: GET {endpoint}')
        return self.client.get(self.URL_BASE + endpoint)
