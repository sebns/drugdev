import os


db_dir = os.environ['SQL_DB_DIR'].rstrip("/")


class ConfigProduction:
    """Debug config."""
    DEBUG = False
    TESTING = False
    SECRET_KEY = bytes.fromhex('883833f999caf43c245950b583432aea252a85a5')
    SQLALCHEMY_DATABASE_URI = f'sqlite:///{db_dir}/data_prod.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SWAGGER_UI_DOC_EXPANSION = 'list'


class ConfigDevelopment(ConfigProduction):
    """Production config."""
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = f'sqlite:///{db_dir}/data_dev.db'


class ConfigTest(ConfigProduction):
    """Test config."""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = f'sqlite:///{db_dir}/data_test.db'
