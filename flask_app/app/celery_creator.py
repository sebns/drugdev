from celery import Celery

from . import celery_config


def make_celery_app() -> Celery:
    """Create Celery object."""
    return Celery(
        'proj',  # used as prefix for auto-generated task names
        broker='redis://redis_svc:6379/0')


def init_celery_app(celery_app: Celery, app) -> None:
    """Initialise Celery object.
    :param celery_app: the Celery app to initialise
    :param app: the Flask app
    """
    celery_app.conf.update(app.config)
    celery_app.config_from_object(celery_config)
    TaskBase = celery_app.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery_app.Task = ContextTask


celery_app = make_celery_app()
