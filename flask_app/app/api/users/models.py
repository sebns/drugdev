import logging
from datetime import datetime

from ... import db

logger = logging.getLogger(__name__)


class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True, nullable=False)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    emails = db.relationship('Email', lazy='select', backref='user')
    created_on = db.Column(db.DateTime, default=datetime.utcnow)
    updated_on = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f"<User(index='{self.id}', username='{self.username}'>"

    def to_dict(self):
        """Convert to a dict (returned on the REST API)."""
        return dict(
            username=self.username,
            first_name=self.first_name,
            last_name=self.last_name,
            emails=[item.email for item in self.emails],
            created_on=self.created_on.timestamp(),
            updated_on=self.updated_on.timestamp(),
        )

    def update_from_dict(self, params: dict):
        """Update parameters from a dict.
        Note: a session.commit will be required afterwards
        :param kwargs: optional user parameters
        """
        for name, value in params.items():
            if value is None:
                continue  # no data (None is the default argparse value)
            logger.debug(f"Setting {self.username}'s param: {name}={value}")
            if name == 'emails':
                # emails should be converted from CSV to list
                self.emails.clear()
                if value != '':
                    for email in value.split(','):
                        email_item = Email(email=email)
                        db.session.add(email_item)
                        self.emails.append(email_item)
            else:
                # no conversion needed
                setattr(self, name, value)


class Email(db.Model):
    __tablename__ = 'email'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, unique=True, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)

    def __repr__(self):
        return f"<Email(index='{self.id}', email='{self.email}'>"
