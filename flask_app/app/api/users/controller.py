import logging
from typing import Optional, Iterable
from datetime import datetime

from .models import User
from ... import db

logger = logging.getLogger(__name__)


def get_all_users() -> dict:
    """Get info for all users.
    :return: info about all users
    """
    users = []
    items: Iterable[User] = db.session.query(User).all()
    for item in items:
        users.append(item.to_dict())
    return dict(users=users)


def get_user(username: str) -> Optional[User]:
    """read user information from database.
    :param username: the user's username
    :return: a User (or None if username does not exist)
    """
    return db.session \
        .query(User) \
        .filter_by(username=username) \
        .first()  # will be None if user does not exists


def create_user(username: str, **kwargs) -> None:
    """Create a user.
    :param username: user's username
    :param kwargs: optional user parameters
    """
    now = datetime.utcnow()
    user = User(username=username, updated_on=now, created_on=now)
    user.update_from_dict(kwargs)
    db.session.add(user)
    db.session.commit()


def update_user(user: User, **kwargs) -> None:
    """Update a user's information.
    :param user: user
    :param kwargs: optional user parameters
    """
    user.update_from_dict(kwargs)
    user.updated_on = datetime.utcnow()
    db.session.commit()


def delete_user(user: User) -> None:
    """Delete a user.
    :param user: user
    """
    db.session.delete(user)
    db.session.commit()
