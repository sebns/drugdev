import logging
from http import HTTPStatus
from flask import jsonify, Response, make_response
from flask_restplus import Resource, Namespace
from flask_restplus.reqparse import RequestParser
from sqlalchemy.exc import SQLAlchemyError

from .controller import (
    get_all_users, get_user, create_user, update_user, delete_user)
from ...celery_tasks import delete_users_created_before


ns = Namespace('Users', path='/users',
               description='Information about users')

logger = logging.getLogger(__name__)


@ns.route('/', endpoint='display')
class InfoUsers(Resource):
    """Class handling users."""
    @ns.doc(
        responses={
            HTTPStatus.OK: 'Success',
            HTTPStatus.INTERNAL_SERVER_ERROR: 'Internal database error'})
    def get(self) -> Response:
        """Get all the users."""
        try:
            data = get_all_users()
        except SQLAlchemyError:
            return make_response(
                jsonify(dict(status='database error')),
                HTTPStatus.INTERNAL_SERVER_ERROR)
        return jsonify(data)

    def delete(self) -> Response:
        """Delete users created more than a minute ago."""
        delete_users_created_before(seconds=60)
        return jsonify(dict(status='Started to delete some users'))


@ns.route('/<username>')
@ns.param('username', description='username of the user')
class InfoUser(Resource):
    """Class handling a single user."""
    parser = RequestParser()
    parser.add_argument('first_name', help="User's first name")
    parser.add_argument('last_name', help="User's last name")
    parser.add_argument('emails', help="User's email addresses (comma-separated)")

    @ns.doc(
        responses={
            HTTPStatus.OK: 'Success',
            HTTPStatus.NOT_FOUND: 'User not found'})
    def get(self, username: str) -> Response:
        """Get info about a user."""
        user = get_user(username)
        if not user:
            return make_response(
                jsonify(dict(status='User not found')), HTTPStatus.NOT_FOUND)
        return jsonify(user.to_dict())

    @ns.doc(
        parser=parser,
        responses={
            HTTPStatus.OK: 'Success',
            HTTPStatus.CONFLICT: 'Username already exists'})
    def post(self, username: str) -> Response:
        """Create a user."""
        args = self.parser.parse_args()
        if get_user(username):
            return make_response(
                jsonify(dict(status='Username already exists')),
                HTTPStatus.CONFLICT)
        create_user(
            username=username,
            emails=args.emails,
            first_name=args.first_name,
            last_name=args.last_name)
        return jsonify(dict(status='user created successfully'))

    @ns.doc(
        parser=parser,
        responses={
            HTTPStatus.OK: 'Success',
            HTTPStatus.NOT_FOUND: 'User not found'})
    def put(self, username: str) -> Response:
        """Update a user."""
        args = self.parser.parse_args()
        user = get_user(username)
        if not user:
            return make_response(
                jsonify(dict(status='User not found')),
                HTTPStatus.NOT_FOUND)
        update_user(
            user=user,
            emails=args.emails,
            first_name=args.first_name,
            last_name=args.last_name)
        return jsonify(dict(status='user updated successfully'))

    @ns.doc(
        responses={
            HTTPStatus.OK: 'Success',
            HTTPStatus.NOT_FOUND: 'User not found'})
    def delete(self, username: str) -> Response:
        """Update a user."""
        user = get_user(username)
        if not user:
            return make_response(
                jsonify(dict(status='User not found')),
                HTTPStatus.NOT_FOUND)
        delete_user(user)
        return jsonify(dict(status='user updated successfully'))
