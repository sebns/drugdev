from flask import Blueprint
from flask_restplus import Api


bp = Blueprint('api', __name__)

api = Api(
    bp, version='1.0', title='DrugDev',
    description='DrugDev simple API',
    default='', default_label='', doc='/',
)

from . import users
api.add_namespace(users.ns)
