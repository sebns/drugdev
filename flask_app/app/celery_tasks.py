import random
import string
from datetime import datetime, timedelta
import logging

from .api.users.controller import create_user
from .api.users.models import User
from .celery_creator import celery_app
from . import db


logger = logging.getLogger(__name__)


@celery_app.task
def delete_users_created_before(seconds: float) -> None:
    """Delete older entries."""
    date_limit = datetime.utcnow() - timedelta(seconds=seconds)
    logger.info(f'Deleting users created before {date_limit}')
    for user in db.session.query(User).all():
        if user.created_on < date_limit:
            logger.info(
                f"Deleting user {user.username} created on: {user.created_on}")
            db.session.delete(user)
    db.session.commit()


@celery_app.task
def make_random_user() -> None:
    """Create a user with random data."""
    create_user(
        first_name=_make_random_string(20),
        last_name=_make_random_string(20),
        username=_make_random_string(50))


def _make_random_string(sz: int) -> str:
    return ''.join(random.choice(string.ascii_lowercase) for i in range(sz))
