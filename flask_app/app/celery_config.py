CELERY_IMPORTS = ()
CELERY_TASK_RESULT_EXPIRES = 10  # We don't need to keep any result
CELERY_TIMEZONE = 'UTC'

CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

CELERYBEAT_SCHEDULE = {
    'test-celery': {
        'task': 'app.celery_tasks.make_random_user',
        'schedule': 15.0,  # seconds
    }
}
