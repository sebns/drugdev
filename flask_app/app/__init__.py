import os
import logging
from flask import Flask, redirect, url_for, Response
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()
logger = logging.getLogger(__name__)


def create_app():
    app = Flask(__name__)

    logging.basicConfig(
        format="%(asctime)s : %(name)s : %(levelname)s : %(message)s",
        level='DEBUG' if app.debug else 'INFO')
    logger = logging.getLogger(__name__)

    env = os.getenv('FLASK_ENV', 'production')
    cfg = 'app.settings.Config' + env.capitalize()
    logger.info('Setting configuration from: ' + cfg)
    app.config.from_object(cfg)

    from . import api
    app.register_blueprint(api.bp, url_prefix='/api/v' + api.api.version)

    logger.info(
        f"Init database {app.config['SQLALCHEMY_DATABASE_URI']}"
        " and create tables if needed")
    db.init_app(app)
    db.create_all(app=app)

    app.add_url_rule('/', 'index', index)

    return app


def index() -> Response:
    """Index page."""
    return redirect(url_for('api.doc'))
