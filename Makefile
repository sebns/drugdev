.PHONY: start stop stop_clean dev test

start: ./data
	@echo "=== Starting DETACHED"
	docker-compose up --build -d

stop:
	@echo "=== Stopping"
	docker-compose down

stop_clean:
	@echo "=== Clean data directory"
	-docker-compose down
	-rm -rf ./data

dev: stop_clean ./data
	@echo "=== Starting with dev environment in DEBUG mode"
	FLASK_DEBUG=1 FLASK_ENV=development \
	  docker-compose up --build

test: stop_clean ./data
	FLASK_ENV=test \
	  docker-compose up --build -d
	@echo "=== Start TESTS"
	docker-compose exec --workdir=/code \
	  flask_app coverage run -m unittest
	@echo "=== Generate HTML report of code coverage"
	docker-compose exec --workdir=/code \
	  flask_app coverage html

./data:
	@echo "=== Creating data directory (for database)"
	mkdir ./data
