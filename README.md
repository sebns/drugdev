# DrugDev simple app

Application using the Flask web framework, SQLAlchemy and Celery.

## Start the app

Use any Linux machine with Docker installed. To start the Flask app, redis, celery and celery-beat containers:

```sh
$ make start
```

The REST API documentation should be accessible from `http://localhost/api/v1.0/`, and the SQLite database will be created in `./data/`.

Note:

- a Celery task is adding a new contact with random data every 15 seconds
- the endpoint `/users` with method `DELETE` is using a Celery task to delete users created more than a minute ago.

To stop the app:

```sh
$ make stop
```

## Tests

To start containers and run the tests:

```sh
$ make test
    ----------------------------------------------------------------------
    Ran 5 tests in 0.364s

    OK
```

Also, the code coverage report will be located in `./data/coverage_html_report/index.html`

## To do

Add more tests. Possibly use pytest HTML reports.
